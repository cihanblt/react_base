export const goLoginPage = () => (dispatch) => {
    return dispatch({ type: 'LOGINPAGE', payload: '' });
};

export const authenticate = (name, pass) => (dispatch) => {
    if (name === 'chn' && pass === '123') {
        return dispatch({ type: 'AUTHOK', payload: { isAuthenticate: true } });
    } else {
        return dispatch({ type: 'AUTHOK', payload: { isAuthenticate: false } });
    }
};

export const logout = () => (dispatch) => {
    return dispatch({ type: 'AUTHLOGOUT', payload: { isAuthenticate: true } });
};