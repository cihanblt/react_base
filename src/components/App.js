import logo from '../logo.svg';
import './App.css';

import React from 'react';
import NavigationBar from './NavigationBar';
import Home from './Home';
import SignUp from './SignUp';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './Login';
import privateRoute from './PrivateRoute';
import Logout from './Logout';
import Dashboard from './dashboard/Dashboard';


const Protected = () => <div ><h3>Protected</h3></div>

class App extends React.Component {
    render() {
        return (
            <BrowserRouter >
                <div>
                <NavigationBar />
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/home" component={Home} />
                        <Route path="/signup" component={SignUp} />
                        <Route path="/login" component={Login} />
                        <Route path="/logout" component={Logout} />
                        <Route path="/protected" component={privateRoute(Protected)} />
                        <Route path="/dashboard" component={Dashboard} />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;