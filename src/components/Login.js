import React, { Component } from 'react';
import './Login.css';
import { connect } from 'react-redux';
import { goLoginPage, authenticate } from '../actions/LoginAction';
import { withRouter } from 'react-router-dom';
import $ from 'jquery';


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputEmail: '',
            inputPassword: ''
        }
    }
    componentWillMount() {
        $('#top-menu').css("display", "none");
    }

    componentDidMount() {
        $('#top-menu').css("display", "none");
    }
    componentWillUnmount() {
        $('#top-menu').css("display", "flex");
    }

    onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state.inputEmail);
        // this.props.goLoginPage();
        this.props.authenticate(this.state.inputEmail, this.state.inputPassword);
    }

    componentWillReceiveProps(nextProps) {
        setTimeout(1000);
        console.log("ok  = " + nextProps.authObj.isAuthenticated);
        if (nextProps.authObj.isAuthenticated) {
            nextProps.history.push('/protected');
        } else {

        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className="ui middle aligned center aligned grid">
                <div className="column">
                    <h2 className="ui teal image header">
                        <img src="https://semantic-ui.com/examples/assets/images/logo.png" className="image" />
                        <div className="content">
                            Log-in to your account
                        </div>
                    </h2>
                    <form className="large ui form" onSubmit={this.onSubmit}>
                        <div className="ui stacked segment">
                            <div className="field">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input type="text" onChange={this.onChange} name="inputEmail" placeholder="E-mail address" />
                                </div>
                            </div>

                            <div className="field">
                                <div className="ui left icon input">
                                    <i className="lock icon"></i>
                                    <input type="password" onChange={this.onChange} id="inputPassword" name="inputPassword" placeholder="Password" required />
                                </div>
                            </div>
                            <button className="ui fluid large teal submit button" type="submit">Login</button>
                        </div>
                        <div className="ui error message">
                            <div className="header">We had some issues</div>
                            <ul className="list">
                                <li>Please enter your first name</li>
                                <li>Please enter your last name</li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>

        )
    }
}
const mapsStateToProps = (state, ownProps) => ({
    loginPage: state.loginStore.loginPage,
    authObj: state.loginStore.authObj
});

const mapsDispatchToProps = (dispatch) => ({
    goLoginPage: () => { dispatch(goLoginPage()) },
    authenticate: (name, pass) => { dispatch(authenticate(name, pass)) }
});
export default withRouter(connect(mapsStateToProps, mapsDispatchToProps)(Login));
