import React,{Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';


class Logout extends Component{
    

    componentWillMount(){
        this.props.history.push('/home');
    }

    render(){
        return(
            <div></div>
        );
    }
}

const mapStateToProps = (state)=>({
    authObj: state.loginStore.authObj
});

const mapDispatchToProps = (dispatch)=>({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Logout));