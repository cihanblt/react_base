import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../actions/LoginAction';


class NavigationBar extends Component {

  componentWillReceiveProps(nextProps) {
    console.log('next prop');
    if (this.props.authObj.isAuthenticated != nextProps.authObj.isAuthenticated) {
      console.log('props is changed');
    }
  }

  logout = () => {
    this.props.logout();
  }

  render() {
    const isAuthSignUpButton = this.props.authObj.isAuthenticated == true ? <NavLink className="item" onClick={this.logout} to="/logout"  >Logout </NavLink>
      :
      <NavLink className="item" to="/signup">Sign Up </NavLink>
      ;
    const isAuthLoginButton = this.props.authObj.isAuthenticated ? '' : <NavLink to="/login" className="item" >Sign In </NavLink>;


    return (
      <div id="top-menu" className="ui fixed inverted menu">
        <div className="menu">
          <NavLink to="/" className="header item">
            <img alt="" className="logo" src="https://semantic-ui.com/examples/assets/images/logo.png" />
            Home
          </NavLink>
          <NavLink to="/home" className="item">Home <span className="sr-only">(current)</span></NavLink>
          <NavLink to="/dashboard" className="item">Dashboard</NavLink>
        </div>
        <div className="right menu">
          <div className="item">
            <form >
              <div className="ui transparent inverted  icon input" >
                <i className="search icon"></i>
                <input type="text" placeholder="Search" />
              </div>
            </form>
          </div>
          {isAuthSignUpButton}
          {isAuthLoginButton}
        </div>
      </div>
    )
  }
}

const mapsStateToProps = (state, ownProps) => ({
  authObj: state.loginStore.authObj
});

const mapsDispatchToProps = (dispatch) => ({
  logout: () => { dispatch(logout()) }
});
export default connect(mapsStateToProps, mapsDispatchToProps)(NavigationBar);
