import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';


export default (Component) =>{
    class PrivateRoute extends React.Component {
        constructor(props) {
            super(props);
            console.log(this.props.authObj);
        }

        render() {
            return (
                this.props.authObj.isAuthenticated ? <Component {...this.props} /> : <Redirect to="/login" />
                )
        };
    }
    const mapsStateToProps = (state) => ({
        authObj: state.loginStore.authObj
    });
    return withRouter(connect(mapsStateToProps, {})(PrivateRoute));
};