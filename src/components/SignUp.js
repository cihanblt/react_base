import React, { Component } from 'react';

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email:'',
            password:''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    onSubmit(e) {
        e.preventDefault();
        console.log(this.state);
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div className="ui text container">
                    <form onSubmit={this.onSubmit} className="ui fluid form">
                    <h4 class="ui top attached block header">
                        SignUp!!
                     </h4>
                    <div className="ui attached segment">
                        <div className="field">
                                <label >Username</label>
                                <input type="text" name="username" value={this.state.username} onChange={this.onChange} />
                                <div class="ui pointing label">
                                Please enter a value
                                </div>
                            </div>
                            <div className="field">
                                <label >E-mail</label>
                                <input type="text" name="email" value={this.state.email} onChange={this.onChange} />
                            </div>
                            <div className="field">
                                <label >Password</label>
                                <input  type="password" name="password" value={this.state.password} onChange={this.onChange} />
                            </div>
                            <div className="field">
                                <button className="ui button primary">
                                    Sign Up
                                </button>
                            </div>
                    </div>
                    </form>
            </div>

        )
    }
}

export default SignUp;