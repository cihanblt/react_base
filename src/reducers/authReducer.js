const LOGIN_PAGE = 'LOGINPAGE';
const AUTH_OK = 'AUTHOK';
const AUTH_LOGOUT = 'AUTHLOGOUT';

const initialState = {
    authObj: {
        isAuthenticated: false
    }
};


export default function authReducer(state = initialState, action = {}) {
    switch (action.type) {
        case LOGIN_PAGE:
            state.loginPage = !state.loginPage;
            return state;
        case AUTH_OK:
            if (action.payload.isAuthenticate) {

                state = Object.assign({}, state, {
                    authObj: {
                        isAuthenticated: true
                    }
                });
                return state;

            } else {
                return state;
            }
        case AUTH_LOGOUT:
            if (action.payload.isAuthenticate) {
                state = Object.assign({}, state, {
                    authObj: {
                        isAuthenticated: false
                    }
                });
            }
            console.log('logout state : ' + state);
            return state;
        default:
            return state;
    }
}